package com.devcamp.jbr530.jbr530;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jbr530Application {

	public static void main(String[] args) {
		SpringApplication.run(Jbr530Application.class, args);
	}

}
