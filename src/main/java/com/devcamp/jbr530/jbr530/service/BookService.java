package com.devcamp.jbr530.jbr530.service;

import java.util.ArrayList;

import com.devcamp.jbr530.jbr530.model.Book;

public class BookService {
    public static ArrayList<Book> getListBook() {
        ArrayList<Book> listAllBook = new ArrayList<>();
        Book book1 = new Book("Math", AuthorService.getListAut1(), 500, 50);
        Book book2 = new Book("Java", AuthorService.getListAut2(), 800, 200);
        Book book3 = new Book("CSS", AuthorService.getListAut3(), 650, 100);
        listAllBook.add(book1);
        listAllBook.add(book2);
        listAllBook.add(book3);
        return listAllBook;
    }
}
