package com.devcamp.jbr530.jbr530.service;

import java.util.ArrayList;

import com.devcamp.jbr530.jbr530.model.Author;

public class AuthorService {
    public static ArrayList<Author> getListAut1() {
        ArrayList<Author> list1 = new ArrayList<>();
        Author author1 = new Author("John", "john@gmail.com", 'M');
        Author author2 = new Author("Mary", "mary@gmail.com", 'F');
        list1.add(author1);
        list1.add(author2);
        return list1;
    }

    public static ArrayList<Author> getListAut2() {
        ArrayList<Author> list2 = new ArrayList<>();
        Author author3 = new Author("David", "david@gmail.com", 'M');
        Author author4 = new Author("Cait", "cait@gmail.com", 'F');
        list2.add(author3);
        list2.add(author4);
        return list2;
    }

    public static ArrayList<Author> getListAut3() {
        ArrayList<Author> list3 = new ArrayList<>();
        Author author5 = new Author("Cris", "cris@gmail.com", 'M');
        Author author6 = new Author("Rose", "rose@gmail.com", 'F');
        list3.add(author5);
        list3.add(author6);
        return list3;
    }

    public static ArrayList<Author> getAllAut() {
        ArrayList<Author> listAll = new ArrayList<>();
        listAll.addAll(getListAut1());
        listAll.addAll(getListAut2());
        listAll.addAll(getListAut3());
        return listAll;
    }
}
