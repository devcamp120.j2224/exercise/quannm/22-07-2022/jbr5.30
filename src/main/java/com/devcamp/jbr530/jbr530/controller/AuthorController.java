package com.devcamp.jbr530.jbr530.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.jbr530.jbr530.model.Author;
import com.devcamp.jbr530.jbr530.service.AuthorService;

@RestController
public class AuthorController {
    @CrossOrigin
    @GetMapping("/author-info")
    public Author requestAuthor(@RequestParam(required = true, name = "email") String email) {
        ArrayList<Author> listAuthor = AuthorService.getAllAut();
        Author authorFound = new Author();
        for (Author author : listAuthor) {
            if (author.getEmail().equals(email)) {
                authorFound = author;
            }
        }
        return authorFound;
    }

    @CrossOrigin
    @GetMapping("/author-gender")
    public ArrayList<Author> requestGender(@RequestParam(required = true, name = "gender") char gender) {
        ArrayList<Author> listAuthor = AuthorService.getAllAut();
        ArrayList<Author> listFound = new ArrayList<>();
        for (Author author : listAuthor) {
            if (author.getGender() == gender) {
                listFound.add(author);
            }
        }
        return listFound;
    }
}
