package com.devcamp.jbr530.jbr530.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.jbr530.jbr530.model.Book;
import com.devcamp.jbr530.jbr530.service.BookService;

@RestController
public class BookController {
    @CrossOrigin
    @GetMapping("/books")
    public ArrayList<Book> getListBook() {
        ArrayList<Book> listBook = BookService.getListBook();
        return listBook;
    }

    @CrossOrigin
    @GetMapping("/book-quantity")
    public ArrayList<Book> requestBookQty(@RequestParam(required = true, name = "quantityNumber") int qty) {
        ArrayList<Book> listBook = BookService.getListBook();
        ArrayList<Book> listFound = new ArrayList<>();
        for (Book book : listBook) {
            if (book.getQty() >= qty) {
                listFound.add(book);
            }
        }
        return listFound;
    }
}
